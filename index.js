const express = require("express")
const path = require("path");
const bodyParser = require('body-parser')

const app = express()
const port = 8000

const jsonParser = bodyParser.json()

const Post = require('./controllers/post.js')

const post = new Post

app.get('/', function(req,res) {
    res.json({
        'Hello': 'world'
    })
})

app.get('/post', post.getPost)
app.get('/post/:index', post.getDetailPost)
app.post('/post', jsonParser, post.insertPost)
app.put('/post/:index', jsonParser, post.updatePost)
app.delete('/post/:index', post.deletePost)


app.listen(8000, () => console.log("Server sukses dijalankan"))