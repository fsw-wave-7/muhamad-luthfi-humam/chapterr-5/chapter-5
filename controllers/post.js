const { successResponse } = require ('../helpers/response.js')
class Post {
    constructor() {
        this.post = [
            {
                'text': 'user pertama',
                'nama_lengkap': 'Mulyana Derajat',
                'username': 'mulder',
                'password': 'inimulder',
                'email': 'mulder@gmail.com'
            },
            {
                'text': 'user kedua',
                'nama_lengkap': 'Asep Kuroko',
                'username': 'askuro',
                'password': 'iniaskuro',
                'email': 'askuro@gmail.com'
            }
        ]
    }

    getPost = (req, res) => {
        succesResponse(
            res,
            200,
            this.post,
            { total: this.post.length }
        )
    }

    getDetailPost = (req, res) => {
        const index = req.params.index
        successResponse(res, 200, this.post[index])
    }

    insertPost = (req, res) => {
        const body = req.body
        
        const param = {
            'text': body.text,
            'nama_lengkap': body.nama,
            'username': body.username,
            'password': body.password,
            'email': body.email
        }

        this.post.push(param)

        successResponse(res, 201, param)
    }

    updatePost = (req, res) => {
        const index = req.params.index
        const body = req.body

        this.post[index].text = body.text
        this.post[index].username = body.username
        
        successResponse(res, 200, this.post[index])
    }

    deletePost = (req, res) => {
        const index = req.params.index

        this.post.splice(index, 1);

        successResponse(res, 200, null)
    }
}

module.exports = Post